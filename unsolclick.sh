#!/bin/sh

uci set network.lan.gateway=10.1.104.2
uci set network.lan.ip6gw=2a00:1508:1:fe4c::104:2

uci set network.guifi=route
uci set network.guifi.interface=lan
uci set network.guifi.target=10.0.0.0
uci set network.guifi.netmask=255.0.0.0
uci set network.guifi.gateway=10.1.104.2

uci delete alfred.alfred.disabled

[ "$(uci get lime.system.hostname)" == "BCNmtjOficinesReunions" ] && uci set batman-adv.bat0.gw_mode=server

uci commit

rm -rf /etc/resolv.conf
echo "nameserver 10.1.104.2" > /etc/resolv.conf

